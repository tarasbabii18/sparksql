import org.apache.spark.sql.SparkSession

object AwardCountSQL {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("AwardCountSQL")
      .getOrCreate
    val tableWithFullNames = spark
      .read.csv("C:\\spark\\workspace\\sparksql\\src\\resources\\Master.csv")
    val tableWithAwards = spark
      .read.csv("C:\\spark\\workspace\\sparksql\\src\\resources\\AwardsPlayers.csv")

    tableWithFullNames.select(tableWithFullNames.col("_c0").as("playerID"),
      tableWithFullNames.col("_c3").as("firstname"),
      tableWithFullNames.col("_c4").as("lastname"))
      .createOrReplaceTempView("tableWithFullNames")

    tableWithAwards.select(tableWithAwards.col("_c0").as("playerID"),
      tableWithAwards.col("_c1").as("awards"))
      .createOrReplaceTempView("tableWithAwards")

    val joinedTables = tableWithAwards.sqlContext.sql("SELECT c.playerID, c.awards, o.firstname, o.lastname " +
      "FROM tableWithAwards c INNER JOIN tableWithFullNames o ON (c.playerID = o.playerID)")
      joinedTables.createOrReplaceTempView("joinedTables")

    val countedAwards =  joinedTables.sqlContext.sql("SELECT playerID, firstname, lastname, COUNT(awards)" +
      " as awards FROM joinedTables GROUP BY playerID, firstname, lastname ORDER BY COUNT(awards) DESC")
      .drop("playerID")
    countedAwards.createOrReplaceTempView("countedAwards")

    val result = countedAwards.sqlContext.sql("SELECT CONCAT(firstname, ' ', lastname, ' - ', awards)" +
      " FROM countedAwards").collect().foreach(println)

    spark.stop()
  }
}
